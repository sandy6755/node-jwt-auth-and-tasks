
'use strict';
module.exports = function (app) {
  var userconroler = require('../controller/user.controller');

    app.route('/signin')
    .post(userconroler.signin)

    app.route('/signup')
    .post(userconroler.signup);
        
    app.route('/getproduct')
    .get(userconroler.list_product)
    .post(userconroler.create_product);
    
  app.route('/getproduct/:getproductid')
    .get(userconroler.read_product)
    .put(userconroler.update_product)
    .delete(userconroler.remove_product);

    app.route('/order')
    .post(userconroler.create_order);
    
  app.route('/order/:orderid')
    .get(userconroler.read_order)
    .put(userconroler.update_order)
    .delete(userconroler.remove_order);

    app.route('/orderlist/:userid')
     .get(userconroler.get_order_list)
};
