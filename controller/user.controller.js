// user.route.js

const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const Task = require('../model/product');
const bcrypt = require('bcrypt');
const User = require('../model/user');
const Order = require('../model/order');

function validatetoken(token, res) {
   jwt.verify(token, 'secretD', (err, authData) => {
      if (err) {
         res.status(403).json("unauthorized request")
      }
   })
}
function verifyToken(req, res) {
   // Get auth header value
   const bearerHeader = req.headers['authorization'];
   // Check if bearer is undefined
   if (typeof bearerHeader !== 'undefined') {
      // Split at the space
      const bearer = bearerHeader.split(' ');
      // Get token from array
      const bearerToken = bearer[1];
      // Set the token
      req.token = bearerToken;

      validatetoken(req.token, res)

      // Next middleware
      //   next();
   } else {
      // Forbidden
      res.status(403).json("unauthorized request")

   }
}

exports.signup = function (req, res) {
   bcrypt.hash(req.body.password, 10, function (err, hash) {
      if (err) {
         return res.status(500).json({
            error: err
         });
      }
      else {
         const user = new User({
            _id: new mongoose.Types.ObjectId(),
            email: req.body.email,
            password: hash,
            username: req.body.username,
         });
         user.save().then(function (result) {
            console.log(result);
            res.status(200).json({
               success: 'New user has been created'
            });
         }).catch(error => {
            res.status(500).json({
               error: err
            });
         });
      }
   });
};
exports.signin = function (req, res) {
   console.log(req.body)
   User.findOne({ email: req.body.email })
      .exec()
      .then(function (user) {
         bcrypt.compare(req.body.password, user.password, function (err, result) {
            if (err) {
               return res.status(401).json({
                  failed: 'Unauthorized Access'
               });
            }
            if (result) {
               const JWTToken = jwt.sign({
                  email: user.email,
                  _id: user._id
               },
                  'secretD',
                  {
                     expiresIn: '2h'
                  });
               return res.status(200).json({
                  success: 'Welcome to the JWT Auth',
                  token: JWTToken,
                  user: user
               });
            }
            return res.status(401).json({
               failed: 'Unauthorized Access'
            });
         });
      })
      .catch(error => {
         res.status(500).json({
            error: error
         });
      });;
}


// product crud

exports.list_product = function (req, res) {
   verifyToken(req, res)
   Task.find({}, function (err, task) {
      if (err)
         res.send(err);
      res.json(task);
   });
};
//create product
exports.create_product = function (req, res) {
   verifyToken(req, res)
   console.log("connect =>", req.body)
   const newtask = new Task(req.body);
   newtask.save(req.body).then(function (result) {
      console.log(result);
      res.status(200).json({
         success: result

      });
   }).catch(error => {
      res.status(500).json({
         error: error
      });
   })
}

//read by id product
exports.read_product = function (req, res) {
   verifyToken(req, res)
   Task.findById(req.params.getproductid, function (err, task) {
      if (err)
         res.send(err);
      res.json(task)
   })
}
//update by id product
exports.update_product = function (req, res) {
   verifyToken(req, res)
   Task.findOneAndUpdate({ _id: req.params.getproductid }, req.body, { new: true }, function (err, task) {
      if (err)
         res.send(err);
      res.json(task)
   })
}

//Delete product
exports.remove_product = function (req, res) {
   verifyToken(req, res)
   Task.remove({
      _id: req.params.getproductid
   }, function (err, task) {
      if (err)
         res.send(err);
      res.json({ message: 'Task successfully deleted' });
   });
};


// order crud 

exports.create_order = function (req, res) {
   verifyToken(req, res)
   User.findById(req.body.user_id, function (err, userdata) {
      console.log(userdata)
      if (err ||userdata == null ) {
         res.json({ message: 'Invalid User Id' });
      } else {
         Task.findById(req.body.product_id, function (err, productdata) {
            if (err ||productdata == null ) {
               res.json({ message: 'Invalid Product Id' });
            } else {
               const newtask = new Order(req.body);
               newtask.save(req.body).then(function (result) {
                  console.log(result,userdata);
                  res.status(200).json({
                     success: result,
                     username: userdata.username,
                     product_detail: productdata
                  });
               }).catch(error => {
                  console.log(error)
                  res.status(500).json({
                     error: error
                  });
               })
            }
         })
      }
   })



}

//read by id order
exports.read_order = function (req, res) {
   verifyToken(req, res)
   Order.findById(req.params.orderid, function (err, task) {
      if (err)
         res.send(err);
      res.json(task)
   })
}
//update by id order
exports.update_order = function (req, res) {
   verifyToken(req, res)
   User.findById(req.body.user_id, function (err, userdata) {
      if (err) {
         res.json({ message: 'Invalid User Id' });
      } else {
         Task.findById(req.body.product_id, function (err, productdata) {
            if (err) {
               res.json({ message: 'Invalid Product Id' });
            } else {
               Order.findOneAndUpdate({ _id: req.params.orderid }, req.body, { new: true }, function (err, task) {
                  if (err)
                     res.send(err);
                  res.status(200).json({
                     success: task,
                     username: userdata.username,
                     product_detail: productdata
                  });
               })
            }
         })
      }
   })


}

//Delete order
exports.remove_order = function (req, res) {
   verifyToken(req, res)
   Order.remove({
      _id: req.params.orderid
   }, function (err, task) {
      if (err)
         res.send(err);
      res.json({ message: 'Task successfully deleted' });
   });
};

//get product by id

exports.get_order_list = function (req, res) {
   verifyToken(req, res)
   Order.find({}, function (err, task) {
      if (err) {
         res.send(err);
      } else {
         User.findById(req.params.userid, function (err, userdata) {
            console.log("order list",task,"\n",userdata)
            let user_order = task.filter((res) => res.user_id == userdata._id)
            res.json(user_order);
         })
      }
   });
};