const express = require('express');
const app = express();
const bodyParser = require('body-parser');
var route = require('./router/approute');
var mongoose = require('mongoose');
let product = require('./model/product');
let order = require('./model/order') 
const PORT = 3000;

mongoose.connect('mongodb://localhost/oms', { useNewUrlParser: true, useUnifiedTopology: true })
.then(res=>console.log("DB connected successfully"))

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
   });
   

app.get('/', function(req, res){
   res.json({
      "hi": "Welcome to the Node express JWT Auth"
   });
});


route(app)

app.listen(PORT, function(){
   console.log('Server is running on Port',PORT);
});
 
