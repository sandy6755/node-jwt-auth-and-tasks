
'use strict'; 

const mongoose = require('mongoose');

var Schema = mongoose.Schema;

const order = new Schema({
    user_id: {type: String, required: true},
    product_id : {type: String, required: true},
 })

 module.exports = mongoose.model('Order', order);