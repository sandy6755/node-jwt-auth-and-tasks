
'use strict'; 

const mongoose = require('mongoose');

var Schema = mongoose.Schema;

const product = new Schema({
    product_name: {type: String, required: true},
    product_price : {type: String, required: true},
    product_brand:{type: String, required: true},
    product_decscription:{type:String,require:true},
 })

 module.exports = mongoose.model('Product', product);